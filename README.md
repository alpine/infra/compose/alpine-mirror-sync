# Alpine Mirror Sync

This is a set of docker services to create an Alpine Linux mirror. It will
automatically keep the mirror updated by listening on `mqtt` and executing
`rsync`. Optionally can also do a partial update per repository.

## Dependencies

 - docker
 - docker compose
 - Traefik (optionally)

## Configuration

### Nginx and Rsyncd

There are default configs for nginx and rsyncd in the `config` directory.
Adjust them if you need custom configuration. There are a few environment
variables that can be set.

- **RSYNC_SRC** rsync source address (default: rsync://rsync.alpinelinux.org/alpine)
- **RSYNC_DST** rsync destination directory (default: /srv/mirror/alpine)
- **RSYNC_OPTS** rsync options ie `--dry-run`
- **RSYNC_PRT** partial rsync, only works when syncing from master (default: FALSE)
- **MIRROR_ROOT** hosts part of the mirror bind mount (default: /srv/mirror)

These can be set by including a `.env` environment file.

### Traefik

There is a default Traefik configuration included in the `docker-compose.yml`
file. To configure the http host name rule create an `.env` file with ie:
TRAEFIK_RULE="Host(`host1.domain.tld`, `host2.domain.tld`)"

### Periodic update by cron

When running in partial sync mode you can use the included sync script which
can be run from crond. Symlink the sync-mirror script to your periodic cron
directory (ie hourly). This will make sure the root will be periodically synced
completely to catch missed files. Do not run in partial sync mode if you do not
know what you are doing.

### Initial sync

When setting up the mirror it is preferred to do a manual initial sync first.
You can run the `init-sync-mirror` script from the contrib directly to manually
run the first sync. This will disable delay updates as this can cause issues on
larger sync jobs.

